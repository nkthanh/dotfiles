install-base:
	brew install vim neovim tree zsh python3 pipenv htop

install-py:
	mkdir -p ~/.config/pip
	ln -sf `pwd`/pip/pip.conf ~/.config/pip

install-vim:
	rm -rf ~/.vimrc
	ln -sf `pwd`/vim/vimrc ~/.vimrc

install-nvim:
	rm -rf ~/.config/nvim
	rm -rf ~/.vim
	ln -sf `pwd`/vim ~/.vim
	ln -sf `pwd`/nvim ~/.config/nvim
	ln -sf `pwd`/vim/colors ~/.config/nvim/colors
	ln -sf `pwd`/nvim/nvimrc ~/.nvimrc
	ln -sf `pwd`/nvim/nvimrc.local ~/.nvimrc.local

install-git:
	ln -sf `pwd`/git/gitconfig ~/.gitconfig

install-tmux:
	ln -sf `pwd`/tmux/tmux.conf ~/.tmux.conf
	ln -sf `pwd`/tmux/tmux.conf.local ~/.tmux.conf.local

install-bash:
	rm -rf ~/.bashrc
	ln -sf `pwd`/bash/bashrc ~/.bashrc

install-zsh:
	rm -rf `pwd`/oh-my-zsh/custom
	ln -sf `pwd`/zsh/zshrc ~/.zshrc
	ln -sf `pwd`/oh-my-zsh ~/.oh-my-zsh
	ln -sf `pwd`/zsh ~/.oh-my-zsh/custom
	ln -sf `pwd`/zsh/aliases.zsh ~/.aliases.zsh
	ln -sf `pwd`/zsh/envs.zsh ~/.envs.zsh

install-terminal:
	sudo mv ~/Library/Preferences/com.apple.Terminal.plist /Library/Preferences/com.apple.Terminal.plist.bak
	sudo ln -sf terminal/com.apple.Terminal.plist ~/Library/Preferences/ 

install-iterm2:
	sudo mv ~/Library/Preferences/com.googlecode.iterm2.plist ~/Library/Preferences/com.googlecode.iterm2.plist.bak
	sudo ln -sf iterm2/com.googlecode.iterm2.plist ~/Library/Preferences/
